-- create a schema/table for the following processing
-- MVS is not providing proper JSON format data, it's reason to load data into "text" column.
CREATE SCHEMA IF NOT EXISTS stage;
CREATE TABLE IF NOT EXISTS stage.raw_texts (raw_text text);

/*
Loading data to DB from the loaded file "mvswantedmt.json"
The file was loaded with curl application via defined script in .gitlab-ci.yml

\COPY command means that we will copy the file from the local container
COPY (without the slash) will try to look for the file on our DB server.
https://stackoverflow.com/questions/51681905/difference-between-copy-and-copy-commands-in-postgresql
*/
\COPY stage.raw_texts FROM mvswantedmt.json;

-- Verify loading to stage.raw_texts
SELECT rt.raw_text
FROM stage.raw_texts rt
LIMIT 5;

-- As MVS provides incorrect jsons we should verify strings
-- We can try converting (text->json) with the following function.
CREATE OR REPLACE FUNCTION try_cast_json(p_in TEXT, p_default JSON DEFAULT NULL)
   RETURNS JSON
AS
$$
BEGIN
  BEGIN
    RETURN $1::JSON;
  EXCEPTION
    WHEN OTHERS THEN
       RETURN '{"error": 1}';
  END;
END;
$$
LANGUAGE plpgsql;

-- Create table with json column
CREATE TABLE IF NOT EXISTS stage.raw_jsons (raw_json json);

-- Trim text string, without coma.
INSERT INTO stage.raw_jsons
SELECT try_cast_json(LEFT(rt.raw_text, LENGTH(rt.raw_text)-1)) AS raw_json
FROM stage.raw_texts rt
WHERE try_cast_json(LEFT(rt.raw_text, LENGTH(rt.raw_text)-1))::text <> '{"error": 1}';

-- Verify loading to stage.raw_jsons
SELECT rj.raw_json
FROM stage.raw_jsons rj
LIMIT 5;

-- Create a temporary table (into "stage" schema to reload data)
-- DROP TABLE stage.claims;
CREATE TABLE IF NOT EXISTS stage.claims
(
    id BIGINT
  , insert_date TIMESTAMP
  , nm CHARACTER VARYING(50)
  , nz CHARACTER VARYING(50)
  , imei CHARACTER VARYING(15)
  , ovd CHARACTER VARYING(200)
  , nk CHARACTER VARYING(50)
  , dk TIMESTAMP
);

-- Convert to columns
INSERT INTO stage.claims
SELECT (rj.raw_json->>'ID')::BIGINT AS id
  , (rj.raw_json->>'INSERT_DATE')::TIMESTAMP AS insert_date
  , (rj.raw_json->>'NM')::CHARACTER VARYING(50) AS nm
  , (rj.raw_json->>'NZ')::CHARACTER VARYING(50) AS nz
  , (rj.raw_json->>'IMEI')::CHARACTER VARYING(15) AS imei
  , (rj.raw_json->>'OVD')::CHARACTER VARYING(200) AS ovd
  , (rj.raw_json->>'NK')::CHARACTER VARYING(50) AS nk
  , (rj.raw_json->>'DK')::TIMESTAMP AS dk
FROM stage.raw_jsons rj;

-- Verify processing
SELECT c.id
, c.insert_date
, c.nm
, c.nz
, c.imei
, c.ovd
, c.nk
, c.dk
FROM stage.claims c
LIMIT 5;

-- Create persistent table
CREATE TABLE IF NOT EXISTS public.claims
(
    id BIGINT
  , insert_date TIMESTAMP
  , nm CHARACTER VARYING(50)
  , nz CHARACTER VARYING(50)
  , imei CHARACTER VARYING(15)
  , ovd CHARACTER VARYING(200)
  , nk CHARACTER VARYING(50)
  , dk TIMESTAMP
);

-- Speed up search with B-Tree index
CREATE INDEX IF NOT EXISTS idx_claims_imei ON public.claims (imei);

-- Add new claims
INSERT INTO public.claims
SELECT t.id
  , t.insert_date
  , t.nm
  , t.nz
  , t.imei
  , t.ovd
  , t.nk
  , t.dk
FROM stage.claims t
WHERE NOT EXISTS (SELECT NULL
                  FROM public.claims c
                  WHERE t.imei = c.imei)
      AND t.imei IS NOT NULL;

-- Drop temporary objects
DROP SCHEMA stage CASCADE;
